/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Lab10.Henry.Sheridan;

import java.util.Calendar;

/**
 *
 * @author 123
 */
public class Demo {
    public static void main(String[] args) {
        
        Calendar dateJoin = Calendar.getInstance();
        dateJoin.set(2021, 0, 22);
        
        Employee employee = new Employee("0001", dateJoin.getTime(), 40000);
        
        EmployeeTool tool = new EmployeeTool();
        
        System.out.println("is promotion due" + tool.isPromotionDueThisYear(employee, true));
        System.out.println("taxes for the year" + tool.calcIncomeTaxForCurrentYear(employee, 0.28));
    }
    
}
